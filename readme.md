# abas-widget-behavior

## Description 
 
Behavior that maps the standart attributes of an Widget, also provides actions to change widget data

## Public Properties 

* id { String } A Unique Widget ID
* width { Number } The Current Width of the Widget can be either one of these values xl, l, m, s, xs, icon
* height { Number } The current Height of the Widget in Pixel
* maxHeight { Number } The maximum height of the Widget
* maxWidth { Number } The maximum width of the Widget
* minHeight { Number } The minimum height of the Widget
* minWidth { Number } The minimum width of the Widget
* title { String } The Title of the Widget
* editMode { Boolean } If this Dashboard is currently being Edited

## Private Properties 

* _abaWidget { Object } Reference for aba-widget element

## Getter and Setter 

* GET abaWidget Returns a reference of aba-widget

## Observer 

* _editModeChanged(new, old) - Observer for editMode

## Public Methods 

* fireAttributesUpdate(attributes[]) - Triggers a update of the attributes. Expects a list of Objects that contain the changed attributes each Object musst provide name and value
    * attributes[] { Object[] } List of attributes that changed
    * attributes[].name { String } Name of the Attribute
    * attributes[].value  { Any } Value of the Attribute
* fireAttributeUpdate(attribute) - Triggers a update of the attribute. Expects a Object that contains the changed attribute it must provide name and value
    *  attribute.name { String } Name of the Attribute
    *  attribute.value { Any } Value of the Attribute

## Messaging, Filters, Context 
* publishTopic - Publish data to topics
    * topic     : String
    * data      : data to send
    * channel   : target channel, default is 'default'
```javascript
    this.publishTopic ({
        topic: '0:1',
        data: {
            id: '(1,2,3)'
        }
    })
```

* subscribeTopics - Subscribe a handler to a topic 
    * topics = []
    * handler
    * channel = 'default'
```javascript
    connectedCallback() {
        super.connectedCallback();
        this.subscribeTopics({
            topics: ['0:1', '2:1'],
            handler: (data, envelope) => { this.work(data, envelope) }
        })
    }
```
* unsubscribeTopics - Unsubscribe handlers from topics. Clean up call.
```javascript
    disconnectedCallback() {
        super.disconnectedCallback();
        this.unsubscribeTopics();
    }
```