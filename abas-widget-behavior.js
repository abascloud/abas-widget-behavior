'use strict';
(() => {


    /**
     * Rgister listeners to any topic recieved from the message bus
     * @param {Object} superClass 
     */
    const TopicSubscriber = (superClass) => class extends superClass {
        constructor() {
            super()
            var subscriptions = []
            this.subscribeTopics = async ({ topics = [], handler, channel = 'default' }) => {
                // just in case, we get no array, we create one, so dashboard works
                if (!_.isArray(topics)) {
                    topics = [topics];
                    //console.log("topic was not an array")
                }
                topics.forEach(async (topic) => {
                    var subscription = postal.subscribe({
                        channel: channel,
                        topic: topic,
                        callback: async (data, envelope) => {
                            handler(data, envelope)
                        }
                    }).enlistPreserved()
                    subscriptions.push(subscription)
                })
            }
            this.unsubscribeTopics = () => {
                subscriptions.forEach((subscription) => {
                    subscription.unsubscribe()
                })
            }
        }

        disconnectedCallback() {
            super.disconnectedCallback()
            this.unsubscribeTopics()
        }
    }

    const TopicPublisher = (superClass) => class extends superClass {

        constructor() {
            super()
            this.publishTopic = async ({ topic, data, channel = 'default', headers = {} }) => {
                // check input
                if (typeof headers !== 'object' || headers.constructor !== Object) {
                    headers = {}
                    console.log("Postal: Headers deleted. No object given.")
                }
                // always send sender's widgetId
                headers['sender'] = this.id;
                // publish
                postal.publish({
                    channel: channel,
                    topic: topic,
                    data: data,
                    headers: headers
                })
            }
        }
    }

    const AttributesBehavior = (superClass) => class extends superClass {
        static get properties() {
            return {
                /**
                 * Unique Widget ID
                 */
                id: {
                    type: String
                }
            }
        }

        constructor() {
            super()
        }

        _isAttribute(attribute) {
            return attribute.hasOwnProperty('name') && attribute.hasOwnProperty('value');
        }

        /**
        * Triggers a update of the attributes. Expects a list of Objects that contain the changed attributes each Object musst provide name and value
        * @param { Object[] } List of attributes that changed
        * @param { Object[].value } name
        * @param { Object[].name } value
        */
        fireAttributesUpdate(attributeList) {
            attributeList.forEach(attribute => {
                if (!this._isAttribute(attribute)) {
                    throw new Error('Attribute malformated!');
                }
            });
            let updateEvent = new CustomEvent('update-attributes', {
                bubbles: true,
                composed: true,
                detail: {
                    attributes: attributeList,
                    widgetID: this.id
                }
            });
            this.dispatchEvent(updateEvent)
        }

        /**
        * Triggers a update of the attribute. Expects a Object that contains the changed attribute it must provide name and value
        * @param {Object} Attribute attribute that changed
        */
        fireAttributeUpdate(attribute) {
            if (!this._isAttribute(attribute)) {
                throw new Error('Attribute malformated!');
            }
            this.fireAttributesUpdate([attribute]);
        }
    }

    /**
     * Behavior that maps the standart attributes of an Widget, also provides actions to change widget data
     * @polymerBehavior WidgetBehavior
     */
    const WidgetBehavior = (superClass) => {
        return class extends
            TopicPublisher(
                TopicSubscriber(
                    AttributesBehavior(
                        Polymer.AbasLocalizeMixin(superClass)))) {

            constructor() {
                super();
            }

            static get properties() {
                return {
                    /**
                     * A Unique Widget ID
                     */
                    id: {
                        type: String
                    },
                    /**
                     * The Current Width of the Widget can be either one of these values xl, l, m, s, xs, icon
                     */
                    width: {
                        type: String
                    },
                    /**
                     * The current Height of the Widget in Pixel
                     */
                    height: {
                        type: Number
                    },
                    /**
                    * The maximum height of the Widget
                    */
                    maxHeight: {
                        type: Number
                    },
                    /**
                    * The maximum width of the Widget
                    */
                    maxWidth: {
                        type: Number
                    },
                    /**
                    * The minimum height of the Widget
                    */
                    minHeight: {
                        type: Number
                    },
                    /**
                    * The minimum width of the Widget
                    */
                    minWidth: {
                        type: Number
                    },
                    /**
                     * The Title of the Widget
                     */
                    title: {
                        type: String
                    },
                    /**
                     * If this Dashboard is currently being Edited
                     */
                    editMode: {
                        type: Boolean,
                        value: false,
                        observer: '_editModeChanged'
                    }
                }
            }

            get abaWidget() {
                if (!this._abaWidget) {
                    this._abaWidget = Polymer.dom(this.shadowRoot).querySelector('aba-widget');
                }
                return this._abaWidget
            }

            get grid() {
                if(!this._grid) this._grid = this.parentElement;
                return this._grid;
            }

            get widgetWidth() {
                let width = parseInt(this.width);
                return (width * this.grid.cellWidth) + ((width - 1) * this.grid.cellMargin);
            }

            get widgetHeight() {
                let height = parseInt(this.height);
                return (height * this.grid.cellHeight) + ((height - 1) * this.grid.cellMargin);
            }

            get contentWidth() {
                return this.widgetWidth - 16;
            }

            get contentHeight() {
                return this.widgetHeight - 16;
            }

            /**
            * LIFECYCLE
            */
            ready() {
                super.ready();
                let slot1 = document.createElement('slot'),
                    slot2 = document.createElement('slot'),
                    fragment = document.createDocumentFragment();
                slot1.setAttribute('name', 'resizer-top');
                fragment.appendChild(slot1);

                slot2.setAttribute('name', 'resizer-bottom');
                fragment.appendChild(slot2);

                this.shadowRoot.appendChild(fragment);
                this.abaWidget.setAttribute('widget-id', this.id);
            }
            /**
            * OBSERVERS
            */
            _editModeChanged(newVal) {
                if (newVal !== undefined) {
                    if (newVal === true) {
                        this.abaWidget.setAttribute('edit-mode', newVal);
                    } else {
                        this.abaWidget.removeAttribute('edit-mode');
                    }
                }
            }
        }
    }

    /**
     * Behavior that maps the standart attributes of an Widget, also provides actions to change widget data
     * @polymerBehavior abaWidgetBehavior
     */
    const WidgetInlineBehavior = (superClass) => {
        return class extends
            TopicPublisher(
                TopicSubscriber(
                    AttributesBehavior(
                        Polymer.AbasLocalizeMixin(superClass)))) {

            constructor() {
                super();
            }

            static get properties() {
                return {
                    /**
                     * A Unique Widget ID
                     */
                    id: {
                        type: String
                    },
                    /**
                     * The Current Width of the Widget can be either one of these values xl, l, m, s, xs, icon
                     */
                    width: {
                        type: String
                    },
                    /**
                     * The current Height of the Widget in Pixel
                     */
                    height: {
                        type: Number
                    },
                    /**
                    * The maximum height of the Widget
                    */
                    maxHeight: {
                        type: Number
                    },
                    /**
                    * The maximum width of the Widget
                    */
                    maxWidth: {
                        type: Number
                    },
                    /**
                    * The minimum height of the Widget
                    */
                    minHeight: {
                        type: Number
                    },
                    /**
                    * The minimum width of the Widget
                    */
                    minWidth: {
                        type: Number
                    },
                    /**
                     * The Title of the Widget
                     */
                    title: {
                        type: String
                    },
                    /**
                     * If this Dashboard is currently being Edited
                     */
                    editMode: {
                        type: Boolean,
                        value: false,
                        observer: '_editModeChanged'
                    }
                }
            }

            /**
            * LIFECYCLE
            */
            ready() {
                super.ready();
                let slot1 = document.createElement('slot'),
                    slot2 = document.createElement('slot'),
                    fragment = document.createDocumentFragment();
                slot1.setAttribute('name', 'resizer-top');
                fragment.appendChild(slot1);

                slot2.setAttribute('name', 'resizer-bottom');
                fragment.appendChild(slot2);

                this.shadowRoot.appendChild(fragment);
            }
        }
    }


    // Register classes
    if (!window.ABAS) {
        window.ABAS = {};
    }
    window.ABAS.TopicPublisher = Polymer.dedupingMixin(TopicPublisher);
    window.ABAS.TopicSubscriber = Polymer.dedupingMixin(TopicSubscriber);
    window.ABAS.WidgetInlineBehavior = Polymer.dedupingMixin(WidgetInlineBehavior);
    window.ABAS.WidgetBehavior = Polymer.dedupingMixin(WidgetBehavior);
    window.ABAS.AttributesBehavior = Polymer.dedupingMixin(AttributesBehavior);
})();